package utils;

import com.github.javafaker.Faker;

public class RandomUser {
    public int gender;
    public String firstName;
    public String lastName;
    public String newEmail;
    public String password;
    public String address1;
    public String address2;
    public String city;
    public String state;
    public int zipcode;
    public String country;
    public int dayOfBirth;
    public int monthOfBirth;
    public int yearOfBirth;
    public int checkBox1;
    public int checkBox2;
    public String homePhone;

    public RandomUser() {
        Faker faker = new Faker();
        gender = faker.random().nextInt(0,1);
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        newEmail = firstName + lastName + yearOfBirth + "@gmail.com";
        zipcode = faker.random().nextInt(10000, 99999);
        dayOfBirth = faker.date().birthday().getDay();
        monthOfBirth = faker.date().birthday().getMonth();
        yearOfBirth = faker.date().birthday().getYear();
        checkBox1 = faker.random().nextInt(1);
        checkBox2 = faker.random().nextInt(1);
        password = "xyz" + yearOfBirth;
        city = faker.address().cityName();
        state = faker.address().state();
        country = faker.country().name();
        homePhone = faker.phoneNumber().subscriberNumber();
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "gender=" + gender +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", newEmail='" + newEmail + '\'' +
                ", password='" + password + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipcode=" + zipcode +
                ", country='" + country + '\'' +
                ", dayOfBirth=" + dayOfBirth +
                ", monthOfBirth=" + monthOfBirth +
                ", yearOfBirth=" + yearOfBirth +
                ", checkBox1=" + checkBox1 +
                ", checkBox2=" + checkBox2 +
                ", homePhone='" + homePhone + '\'' +
                '}';
    }
}

