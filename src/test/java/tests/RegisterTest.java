package tests;

import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @FindBy(css="#submitAccount")
    WebElement register;

    @Test
    void shouldRegisterNewUserWhenAllMandatoryDataIsProvided() throws InterruptedException {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        homePage.goToSignInPage();

        RandomUser user = new RandomUser();
        System.out.println(user);

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.goToRegisterForm(user.newEmail);

        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.registerUser(user);

        register.click();

    }
}
