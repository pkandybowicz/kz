package tests;


import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.w3c.dom.html.HTMLInputElement;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class LoginTest extends BaseTest {


    @Test
    public void shouldRedirectedToMyAccountWhenCorrectCredentialsAreUsed () throws InterruptedException{
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        homePage.goToRegisterForm();

        Assertions.assertEquals("MY ACCOUNT", driver.findElement(By.xpath("//div[@id='center_column']/h1")).getText());

        //implementacja testu (korzystamy z metod z LoginPega i myAccountPage)
    }


}
