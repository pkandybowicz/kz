package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//div/a[@class='login']")
    WebElement signInButton;

    @FindBy(css = "#email")
    WebElement myEmail;

    @FindBy(css = "#passwd")
    WebElement myPassword;

    @FindBy(css="#SubmitLogin")
    WebElement signInButton2;

    @FindBy(id="email_create")
    WebElement registerEmailBox;

    @FindBy(id="SubmitCreate")
    WebElement createAccountButton;


    public LoginPage(WebDriver driverIn, WebDriverWait waitIn){
        super(driverIn, waitIn);
    }

    public void goToRegisterForm(String email) {
//        signInButton.click();
//        myEmail.sendKeys("test@softie.pl");
//        myPassword.sendKeys("1qaz!QAZ");
//        signInButton2.click();
        wait.until(ExpectedConditions.elementToBeClickable(registerEmailBox));
        registerEmailBox.sendKeys(email);
        wait.until(ExpectedConditions.elementToBeClickable(createAccountButton));
        createAccountButton.click();
    }

}
