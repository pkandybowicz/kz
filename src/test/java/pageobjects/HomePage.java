package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.BaseTest;

public class HomePage extends BasePage {

    @FindBy(xpath = "//div/a[@class='login']")
    WebElement signInButton;

    @FindBy(css = "#email")
    WebElement myEmail;

    @FindBy(css = "#passwd")
    WebElement myPassword;

    @FindBy(css = "#SubmitLogin")
    WebElement signInButton2;

    public HomePage(WebDriver driverIn, WebDriverWait waitIn) {
        super(driverIn, waitIn);
    }

    public void openPage() {
        driver.get(BASE_URL + "index.php");
    }

    public void goToRegisterForm() {
        signInButton.click();
        myEmail.sendKeys("test@softie.pl");
        myPassword.sendKeys("1qaz!QAZ");
        signInButton2.click();


    }
}
