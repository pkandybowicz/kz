package pageobjects;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.html.HTMLInputElement;
import utils.RandomUser;

public class RegisterPage extends BasePage {

    @FindBy(css="#id_gender1")
    WebElement gender1;

    @FindBy(css="#id_gender2")
    WebElement gender2;

    @FindBy(id="customer_firstname")
    WebElement customerFirstName;

    @FindBy(id="customer_lastname")
    WebElement customerLastName;

    @FindBy(id="passwd")
    WebElement password;

    @FindBy(id="address1")
    WebElement address1;

    @FindBy(id="address2")
    WebElement address2;

    @FindBy(id="city")
    WebElement city;

    @FindBy(id="postcode")
    WebElement zipCode;

    @FindBy(id="phone")
    WebElement homePhone;

    @FindBy(id="days")
    WebElement dayOfBirth;

    @FindBy(id="months")
    WebElement monthOfBirth;

    @FindBy(id="years")
    WebElement yearOfBirth;

    @FindBy(css="#uniform-newsletter")
    WebElement checkBox1;

    @FindBy(css="#uniform-optin")
    WebElement checkBox2;

    @FindBy(id="id_state")
    WebElement state;

    @FindBy(id="id_country")
    WebElement country;

    @FindBy(css = "#SubmitCreate")
    WebElement createAccount;

    @FindBy(xpath = "//div[@id='center_column']/h1")
    WebElement myAccount;

    @FindBy(css="#submitAccount")
    WebElement register;


    public RegisterPage(WebDriver driverIn, WebDriverWait waitIn){
        super(driverIn, waitIn);
    }

    public void registerUser(RandomUser user) {
        if (user.gender == 0)
            gender1.click();
        else
            gender2.click();
        customerFirstName.sendKeys(user.firstName);
        customerLastName.sendKeys(user.lastName);
        password.sendKeys(user.password);
        Select day = new Select(dayOfBirth);
        day.selectByIndex(user.dayOfBirth);
        Select month = new Select(monthOfBirth);
        month.selectByIndex(user.monthOfBirth);
        Select year = new Select(yearOfBirth);
        year.selectByIndex(user.yearOfBirth);
        address1.sendKeys(user.address1);
        address2.sendKeys(user.address2);
        city.sendKeys(user.city);
        state.click();
        zipCode.sendKeys();
        country.click();
        checkBox1.click();
        checkBox2.click();
        homePhone.sendKeys(user.homePhone);

    }
}
